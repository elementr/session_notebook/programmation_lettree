---
title: "Représentation graphique d'une variable discrétisée"
author: Hugues Pecout
subtitle: "Fonctions R-base & package ggplot2"
date: "`r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: kate
    number_sections: true
---



# Jeu de données

Ce document présente la discrétisation d'une variable numérique (`Sepal.Length`) en 4 classes qualitatives ordinales. Cette variable est issue du **jeu de donnée exemple `iris`**, qui contient `r nrow(iris)` individus décrits par `r ncol(mtcars)` variables :

```{r, echo = FALSE}

library(kableExtra)
knitr::kable(iris) %>%
  kable_styling("striped", full_width = F) %>% 
  scroll_box(height = "200px", width= "500px")
```




## Description de la variable `Sepal.Length`

Les valeurs centrales de la distribution de la variable `Sepal.Length` :

```{r, echo = FALSE}
summary(iris$Sepal.Length)
```

## Histogramme de distribution

```{r, echo = FALSE}
hist(iris$Sepal.Length)
```


## Bornes de classes

Nous faisons le choix d'une **dicrétisation par amplitude égale**, à l'exclusion des bornes extrêmes.

```{r}
bornes_des_classes <- c(min(iris$Sepal.Length), 5, 6, 7, 
                        max(iris$Sepal.Length))
```

## Dénominations des classes.

```{r}
nom_des_classes <- c("Trés petites","petites","grandes","Très grandes")
```

## Discrétisation de la variable 'Sepal.length'

```{r}
iris$classe  <- cut(iris$Sepal.Length,
                    breaks = bornes_des_classes,
                    labels = nom_des_classes,
                    include.lowest = TRUE)
```


# Représentation de la distribution

```{r, echo =FALSE}
library(ggplot2)
```

## Graphique en barre



```{r}
ggplot(data = iris, aes(x = classe)) +
  geom_bar() +
  xlab("") + 
  ylab("") + 
  ggtitle("Répartition par classe") + 
  theme(plot.title = element_text(size=27), 
        axis.text=element_text(size=19))  
```
