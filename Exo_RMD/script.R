#------------------------------------------------------------------------#
#       Titre: Fonctions R-base & package ggplot2
#       Sous-titre: Fonctions R-base & package ggplot2
#       Auteur : Vous
#       Date: courante
#------------------------------------------------------------------------#

# Partie 1. Discrétisation la variable Sepal.Length (données iris) ------#

## 1.2. Statistiques sur la série statistique 'Sepal.Length'
#### -> Montrez seulement la sortie graphique
summary(iris$Sepal.Length)

## 1.3. Histogramme de la distribution
hist(iris$Sepal.Length)

## 1.4. Choix des bornes de classes (amplitude égale)
bornes_des_classes <- c(min(iris$Sepal.Length), 5, 6, 7, 
                        max(iris$Sepal.Length))

## 1.5. Choix des noms de classes
nom_des_classes <- c("Trés petites","petites","grandes","Très grandes")

## 1.6. Discrétisation de la variable 'Sepal.length'
iris$classe  <- cut(iris$Sepal.Length,
                    breaks = bornes_des_classes,
                    labels = nom_des_classes,
                    include.lowest = TRUE)

# Partie 2. Représentation de la distribution selon la discrétisation ---#

## 2.1. Bibliothèque pour la représentation graphique
library(ggplot2)

## 2.2. Représentation graphique de la distribution
ggplot(data = iris, aes(x = classe)) +  # Choix des données et de la variable à représenter
  geom_bar() +  # Choix du type de représentation
  xlab("") +    # Suppression du nom de l'axe x
  ylab("") +    # Suppression du nom de l'axe y
  ggtitle("Répartition par classe") +  # Titre du graphique
  theme(plot.title = element_text(size=27),  # Taille de la police titre 
        axis.text=element_text(size=19))     # Taille des labels d'axe